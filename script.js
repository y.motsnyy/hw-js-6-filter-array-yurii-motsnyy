'use strict';

const currentArr = ['hello', 'world', 23, '23', null];
const currentType = 'string';

function filterBy(arr, type) {
    let newArr = arr.filter(function (item) {
      return typeof item !== type;
        });
        return newArr;
    };

    const nonString = filterBy(currentArr, currentType);
    console.log(nonString);

    const nonNumbers = filterBy(currentArr, 'number');
    console.log(nonNumbers);

    const nonBooleans = filterBy(currentArr, 'boolean');
    console.log(nonBooleans);



    
    

